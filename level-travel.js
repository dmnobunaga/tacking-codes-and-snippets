var queryString = function () {
    var query_string = {};
    var query = window.location.search.substring(1);
    var vars = query.split('&');
    for (var i in vars) {
        var  pair = vars[i].split("=");
        if (typeof query_string[pair[0]] === "undefined") {
            query_string[pair[0]] = decodeURIComponent(pair[1]);
        } else if (typeof query_string[pair[0]] === "string") {
            var arr = [query_string[pair[0]], decodeURIComponent(pair[1])];
            query_string[pair[0]] = arr;
        } else {
            query_string[pair[0]].push(decodeURIComponent(pair[1]));
        }
    }
    return query_string;
}();

if (queryString['utm_campaign']) {
    var utm_campaign = queryString['utm_campaign'].split('|');
    var campaign = utm_campaign[0];
}

if (queryString['utm_content']) {
    var utm_content = queryString['utm_content'].split('|');
    var utm_content_item = utm_content[0].split('_');
    var adgroup_id = utm_content_item[0];
    var target_id = utm_content_item[1];
    var creative_id = utm_content_item[2];
    var adgroup = `${adgroup_id}`;
    var target = `${target_id}`;
    var creative = `${creative_id}`;
}

if (queryString['utm_term']) {
    var utm_term = queryString['utm_term'].split('|');
    var gclid = utm_term;
}
var xpath = "//a[contains(text(),'Скачать приложение')]";
var matchingElement = document.evaluate(xpath, document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
var trackingUrl = decodeURIComponent("https://leveltravel.onelink.me/DJIS?pid=Sociaro_GoogleAds&af_prt=SOCIARO&c=" + campaign + "&af_keywords=" + target + "&af_adset_id=" + adgroup + "_" + target + "_" + creative + "&af_ad_id=" + gclid + "&af_web_dp=https%3A%2F%2Flevel.media%2Flevel-travel-app%2F";);
matchingElement.href = trackingUrl;